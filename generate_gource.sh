#!/bin/bash
#$(date +'%d/%m/%Y')
DATE=`date +%Y-%m-%d-%H:%M`

gource --hide dirnames --seconds-per-day 3 --user-filter "Marius Wichtner | Yannick | Yannick Lamprecht" --start-date '2017-10-20' --stop-date '2018-02-15' --title Smart-Mail \
--git-branch master --logo smartmail\@fra-uas.de/chrome/content/images/unisex64.png --auto-skip-seconds 1 \
-1280x720 -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p \
-crf 1 -threads 0 -bf 0 gource_${DATE}.mp4
