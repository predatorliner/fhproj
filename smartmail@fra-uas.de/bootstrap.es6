/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


let documentURLs =
    [
        "chrome://messenger/content/messengercompose/messengercompose.xul", // Compose message window
        "chrome://messenger/content/addressbook/abNewCardDialog.xul", // New contact
        "chrome://messenger/content/addressbook/abEditCardDialog.xul" // Edit contact
    ];

function startup (data, reason) {
    // console.log ('SmartMail startup', data, reason);
    // Listen for new windows opened. We are particularly interested in the compose window (see `windowListener`).
    Components.utils.import ('resource://gre/modules/Services.jsm');
    Services.wm.addListener (windowListener);
}

function shutdown (data, reason) {
    // Ignore normal shutdowns
    if (reason === APP_SHUTDOWN) { return; }

    // Detach event listeners and clean up modules.
    Components.utils.import ('resource://gre/modules/Services.jsm');
    Services.wm.removeListener (windowListener);
    Services.obs.notifyObservers (null, "chrome-flush-caches", null);
    Components.utils.unload ('chrome://smartmail/content/SmartMail.js');
}

function install (data, reason) {}

function uninstall (data, reason) {}

let windowListener = {
    onOpenWindow: (aWindow) => {
        // console.log ('onOpenWindow');
        let domWindow = aWindow.QueryInterface (Components.interfaces.nsIInterfaceRequestor).getInterface (Components.interfaces.nsIDOMWindow);
        domWindow.addEventListener ('load', function handler (event) {
            // Remove load window listener
            domWindow.removeEventListener ('load', handler, false);

            for (let url of documentURLs) {
                if (event.target.URL === url) {
                    // console.log('GOTCHA!!! url match', event);
                    // Relevant document found, start up smart mail!
                    Components.utils.import ('chrome://smartmail/content/SmartMail.js');
                    console.log ('after import');
                    // console.log ('smartMail', smartMail);
                    smartMail.init (domWindow);
                    break;
                }
            }
        }, false);
    },

    onCloseWindow: function (aWindow) {},

    onWindowTitleChange: function (aWindow, aTitle) {}
};