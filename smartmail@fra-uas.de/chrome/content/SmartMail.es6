/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


let EXPORTED_SYMBOLS = ["smartMail"];

let chrome = {
    Cu: Components.utils,
    Cc: Components.classes,
    Ci: Components.interfaces,
};

let Cu = chrome.Cu;
let Cc = chrome.Cc;
let Ci = chrome.Ci;

Cu.import ("resource://gre/modules/Services.jsm");
Cu.import ("resource://gre/modules/Console.jsm");
Cu.import ("resource://gre/modules/osfile.jsm");
Cu.import ("resource://gre/modules/FileUtils.jsm");
Cu.import ("resource://gre/modules/Promise.jsm");

const {require} = Cu.import ("resource://gre/modules/commonjs/toolkit/require.js", {});

console.log ('1&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
let {NameRepository}         = require ("./lib/repositories/NameRepository");
// let {NameRepository} = Cu.import ("chrome://smartmail/content/lib/repositories/NameRepository.js");
console.log ('2&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
let {SettingsRepository}     = require ("./lib/repositories/SettingsRepository");
let {SmartMailController}    = require ("./lib/controllers/SmartMailController");
let {ConversationRepository} = require ("./lib/repositories/ConversationRepository");
let {ConversationService}    = require ("./lib/services/ConversationService");
let {SettingsService}        = require ("./lib/services/SettingsService");
let {HttpClient}             = require ("./lib/com/HttpClient");
let {Config}                 = require ("./lib/app/Config");
let {UpdateService}          = require ("./lib/services/UpdateService");


let {AnalyticsService}       = require ("./lib/services/AnalyticsService");
let {AnalyticsRepository}    = require ("./lib/repositories/AnalyticsRepository");

let {NameService}            = require ("./lib/services/NameService");
let {setTimeout}             = require ("sdk/timers");
let {Repository}             = require ("./lib/repositories/Repository");
let {GenderFromServiceOrNameController} = require ("./lib/controllers/GenderFromServiceOrNameController");
let {GenderButtonFactory}    = require ("./lib/controllers/GenderButtonFactory");

let {AddressBookDialogController} = require ("./lib/controllers/AddressBookDialogController");

let dbConfig = {
    chrome: chrome,
    connectionString: chromeURIToFilePath ("chrome://smartmail/content/databases/smartmail.sqlite")
};

let smartMailBackendBaseUri = "https://smartmail-backend.herokuapp.com";

let conversationRepository = new ConversationRepository (dbConfig);
let conversationService = new ConversationService (conversationRepository);

let analyticsRepository = new AnalyticsRepository (dbConfig);
let settingsRepository = new SettingsRepository (dbConfig);
let httpClient = new HttpClient (chrome, smartMailBackendBaseUri);
let analyticsService = new AnalyticsService (analyticsRepository, httpClient);
let genderbuttonFactory = new GenderButtonFactory ();

let genericRepo = new Repository (dbConfig);
let updateService = new UpdateService (genericRepo);
let settingsService = new SettingsService (settingsRepository);

/**
 * Parses a chrome URI into a file path.
 * @param {string} chromeURI
 * @returns {string} The file path.
 */
function chromeURIToFilePath (chromeURI) {
    return Cc['@mozilla.org/chrome/chrome-registry;1']
        .getService (Ci.nsIChromeRegistry)
        .convertChromeURL (Services.io.newURI (chromeURI, null, null))
        .QueryInterface (Ci.nsIFileURL)
        .file
        .path;
}

/**
 * Path to names.sqlite downloaded from the backend.
 * @type {string}
 */
const DB_FROM_BACKEND_FILE_PATH = FileUtils.getFile ("UChrm", ["databases", "names.sqlite"]).path;

let disableConsole = (domWindow) => {
    console.log = () => {
    };
    domWindow.console.log = () => {
    };
};

let debug = (domWindow) => {
    if (!Config.isDebug) {
        disableConsole (domWindow);
    }
};

let smartMail = {
    init (domWindow) {
        debug (domWindow);

        let document = domWindow.document;

        OS.File.exists (DB_FROM_BACKEND_FILE_PATH).then (function (fileExists) {
            fileExists ? console.log ("Using names database from server: " + DB_FROM_BACKEND_FILE_PATH) : console.log ("Database file from server does not exist. Falling back to default.");

            let nameRepository = new NameRepository ({
                chrome: chrome,
                connectionString: fileExists ? DB_FROM_BACKEND_FILE_PATH : chromeURIToFilePath ("chrome://smartmail/content/databases/names.sqlite")
            });

            let nameService = new NameService (nameRepository);
            let genderFromServiceOrNameController = new GenderFromServiceOrNameController (conversationService, nameService);
            let addressBookDialogController = new AddressBookDialogController (genderFromServiceOrNameController, genderbuttonFactory, conversationService, analyticsService);

            // conversationService, nameService, analyticsService, setTimeout, genericRepo, addressBookDialogController, genderFromServiceOrNameController, factory
            let smartMailController = new SmartMailController (conversationService, nameService, analyticsService, settingsService, addressBookDialogController, genderFromServiceOrNameController, genderbuttonFactory);
            smartMailController.startup (document);

            updateService.checkForNamesDBUpdates (DB_FROM_BACKEND_FILE_PATH);
        })
    }
};


