export class TestUtils {
    /**
     * Adds a property to the global window scope
     * This works since every tests runs in its own process
     * @param {string} propertyName
     * @param {Object} value
     */
    static defineWindowProperty(propertyName, value) {
        Object.defineProperty(window, propertyName, {
            writable: true,
            value: value
        })
    }
}