let {CollUtils} = require("./CollUtils.js");

describe("CollUtils", () => {
    describe("isNullorEmpty", () => {
        test("should return true if empty", async () => {
            expect(CollUtils.isNullOrEmpty([])).toBe(true)
        });
        test("should return true if null", async () => {
            expect(CollUtils.isNullOrEmpty(null)).toBe(true)
        });
        test("should return false if not empty", async () => {
            expect(CollUtils.isNullOrEmpty([{"test": "test"}])).toBe(false);
        });
    });
});

