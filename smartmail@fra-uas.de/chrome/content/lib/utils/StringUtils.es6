/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


export class StringUtils {
    /**
     * Extracts email address from recipient line
     *
     * @param recipientLine  field value
     * @returns {string}     plain and trimmed email address
     */
    static extractEmailAddress (recipientLine) {
        let regexp = /<([^<>]+@[^<>]+)>/;

        if (regexp.test (recipientLine)) {
            recipientLine = recipientLine.match (regexp) [1];
        }
        return recipientLine.trim ();
    }

    static isEmptyOrWhiteSpace(input) {
        if (typeof input === 'undefined' || input == null) return true;
        return input.replace(/\s/g, '').length < 1;
    }

    static findFirstname(recipientLine) {
        let r = recipientLine;
        r = removeTitles(r);
        r = replaceAllSpecialCharsWithSpace(r);
        r = removeContentOfParenthese(r);
        r = replaceAllDotsWithSpace(r);
        r = removeAllInBetweenBrackets(r);
        r = removeLaceBraceWhenStartingWith(r);
        r = splitAtAtSignAndReturnFirstElement(r);
        r = splitAtLaceBraceAndReturnFirstElement(r);
        r = r.trim();
        r = filterReversedNameAndAddHivenToDoubleName(r);
        r = replaceFirstOccuringSpaceWhenWordlengthThree(r);
        return getFirstName(r);
    }

    static findFullName(recipientLine) {
        let r = recipientLine;
        r = replaceAllSpecialCharsWithSpace(r);
        r = removeContentOfParenthese(r);
        r = removeAllInBetweenBrackets(r);
        r = removeLaceBraceWhenStartingWith(r);
        r = splitAtAtSignAndReturnFirstElement(r);
        r = splitAtLaceBraceAndReturnFirstElement(r);
        r = r.trim();
        r = reorderReversedNames(r);
        r = replaceAllDotsWithNoSpace(r);
        r = removeEverythingBehingOpenBracket(r);
        r = r.trim();
        return r;
    }

    static isStringEmptyOrNull = str => str == null || str.trim() === "";


    static findSalutation (messageBodyText) {
        let regexp = /^[ \n]*([^,\n<]*)[,\n<]/;

        if (regexp.test (messageBodyText)) {
            let match = messageBodyText.match (regexp);
            return match[1];
        } else {
            return "";
        }
    }

    static findFarewell (messageBodyText) {
        let farewellRegExp = /(((?!\n)[\w\s,]*)\n?(<br>)?([\w\s,]*)$)/gi;

        if (farewellRegExp.test (messageBodyText)) {
            let match = messageBodyText.match (farewellRegExp);
            return match[0];
        } else {
            return "";
        }
    }

    static findMessageContent (messageBodyText) {
        let salutation = this.findSalutation (messageBodyText);
        messageBodyText = messageBodyText.replace (salutation, "");
        messageBodyText = removeHeadingBreaksAndNewlines (messageBodyText);
        return messageBodyText;
    }
}


let reorderReversedNames = str => {
    if (/( ){2,}/.test(str)) {
        let firstSecond = str.replace(/\w+( ){2,}/, "");
        let lastName = str.replace(/( ){2,}.+/, "");
        return firstSecond + " " + lastName;
    }
    return str;
};


let removeHeadingBreaksAndNewlines = str => str.replace(/^((<br>|,)(\n|<br>)+)*/gi, "");

let removeContentOfParenthese = str => str.replace(/\(.+\)/, "");
let removeEverythingBehingOpenBracket = str => str.replace(/\(.{1,}$/, "");
let replaceAllDotsWithNoSpace = str => str.replace(/\.(?! )/g, " ");
let getFirstName = str => str.replace(/-/, ",").split(" ")[0];
let removeTitles = str => str.replace(/(Prof|Dr|Mr|Ms|Mrs|Herr|Frau)/g, "");
let replaceAllSpecialCharsWithSpace = str => str.replace(/[_'`´,\d]+/g, " ");
let replaceAllDotsWithSpace = str => str.replace(/[.\d]+/g, " ");
let removeAllInBetweenBrackets = str => str.replace(/\(\w+\)/, "");
let removeLaceBraceWhenStartingWith = str => /^<\w+/.test(str) ? str.replace(/</, "") : str;
let filterReversedNameAndAddHivenToDoubleName = str => {
    return /( ){2,}/.test(str) ? str.replace(/\w+( ){2,}/, "").replace(/ /, "-") : str;
};
let replaceFirstOccuringSpaceWhenWordlengthThree = str => {
    return str.split(/ /).length === 3 ? str.replaceAt(str.indexOf(" "), "-") : str;
};
let splitAtAtSignAndReturnFirstElement = str => str.split(/@/)[0];
let splitAtLaceBraceAndReturnFirstElement = str => str.split(/</)[0];


String.prototype.replaceAt = function (index, replacement) {
    return this.substr (0, index) + replacement + this.substr (index + replacement.length);
};