let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

export class FileUtils {

    constructor(chrome) {
        this.Cc = chrome.Cc;
        this.Ci = chrome.Ci;
        this.Services = chrome.Cu.import("resource://gre/modules/Services.jsm").Services;
        this.FileUtils = chrome.Cu.import("resource://gre/modules/FileUtils.jsm").FileUtils;
    }

    copyToLocationIfFileNotExistent(fileFrom, fileTo) {
        if (!fileTo.exists()) {
            fileFrom.copyTo(fileTo.leafName, "")
        }
    }

    getFileNameFromConnectionString(chromeUrl) {
        return this.getFileFromConnectionString(chromeUrl).path;
    }

    getFileFromConnectionString(chromeUrl) {
        let uri = this.Services.io.newURI(chromeUrl, null, null);
        let filename = this.Cc['@mozilla.org/chrome/chrome-registry;1'].getService(this.Ci.nsIChromeRegistry);
        return filename.convertChromeURL(uri).QueryInterface(this.Ci.nsIFileURL).file;
    }
}


/**
 * Calculates the SHA256 hash for a given file.
 * @param {string} filePath
 * @returns {string} The hex SHA256 hash.
 */
export function SHA256HashFor(filePath) {
    const {Cu, Cc, Ci} = require("chrome");

    Cu.import("resource://gre/modules/Services.jsm");
    Cu.import("resource://gre/modules/osfile.jsm");

    // https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Reference/Interface/nsICryptoHash#Computing_the_Hash_of_a_File
    let file = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
    file.initWithPath(filePath);

    let fileInputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
    fileInputStream.init(file, 0x01, 0o0444, 0);

    let ch = Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);
    ch.init(ch.SHA256);
    const PR_UINT32_MAX = 0xffffffff;
    ch.updateFromStream(fileInputStream, PR_UINT32_MAX);
    let hash = ch.finish(false);

    function toHexString(charCode) {
        return ("0" + charCode.toString(16)).slice(-2);
    }

    return Array.from(hash, (c, i) => toHexString(hash.charCodeAt(i))).join("");
}