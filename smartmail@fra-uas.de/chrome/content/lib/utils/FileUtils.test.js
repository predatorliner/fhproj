var {FileUtils} = require("./FileUtils");

describe("FileUtils", () => {
    let imp;
    let chrome;
    let fileFrom;
    let fileTo;
    let uri;
    let filename;
    beforeEach(() => {
        imp = {
            FileUtils: {},
            Services: {
                io: {
                    newURI: jest.fn()
                },
            }
        };

        filename = {
            convertChromeURL: jest.fn(() => {
                return {
                    QueryInterface: jest.fn(() => {
                        return {
                            file: {path: "dfdf"}
                        }
                    })
                }
            })
        };

        chrome = {
            Cu: {
                import: () => {
                    return imp;
                }
            },
            Cc: {
                '@mozilla.org/chrome/chrome-registry;1': {
                    getService: jest.fn(() => {
                        return filename
                    })
                }
            },
            Ci: {
                nsIChromeRegistry: {}
            }
        };
        fileTo = {
            exists: () => {
                return true
            }
        };
        fileFrom = {
            copyTo: jest.fn()
        }

    });

    function CreateFileUtils() {
        return new FileUtils(chrome);
    }

    test("FileUtils constructor works", () => {
        let fileUtils = CreateFileUtils();
    });

    test("copyToLocationIfFileNotExistent copyTo true works", () => {
        let fileUtils = CreateFileUtils();

        fileUtils.copyToLocationIfFileNotExistent(fileFrom, fileTo);

        expect(fileFrom.copyTo).not.toHaveBeenCalled()
    });

    test("copyToLocationIfFileNotExistent copyTo false works", () => {
        let fileUtils = CreateFileUtils();
        fileTo.exists = () => {
            return false
        };
        fileUtils.copyToLocationIfFileNotExistent(fileFrom, fileTo);
        expect(fileFrom.copyTo).toHaveBeenCalled()
    });

    test("getFileFromConnectionString works", () => {
        let fileUtils = CreateFileUtils();
        let chromeURL = "chrome//url";

        fileUtils.getFileFromConnectionString(chromeURL);

        expect(filename.convertChromeURL).toHaveBeenCalled()
    })
});


