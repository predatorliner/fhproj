export class DOMUtils {
    static getDocumentRoot(domElement) {
        let doc = domElement;
        while(doc.parentNode !== null){
            doc = doc.parentNode;
        }
        return doc;
    }

    static getDocumentRootURL(domElement){
        let doc = this.getDocumentRoot(domElement);
        return doc.URL;
    }
}
