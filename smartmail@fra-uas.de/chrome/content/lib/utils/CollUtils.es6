/**
 * Collection Utilities
 */
export class CollUtils {
    static isNullOrEmpty(array) {
        return array === null || array.length === 0
    }

}