/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


let {StringUtils} = require ("./StringUtils");

describe('StringUtils', () => {
    describe ("extract email address", () => {
        test ('should return email address from valid entry', () => {
            expect (StringUtils.extractEmailAddress ("Franz Ferdinant <franz@franz.de>")).toBe ("franz@franz.de");
        });
        test ('should return email address without actual real name in front', () => {
            expect (StringUtils.extractEmailAddress ("<test@test.com>")).toBe ("test@test.com");
        });
        test ('should return email address containing multiple angle brackets', () => {
            expect (StringUtils.extractEmailAddress ("Franz <Ferdinant> <franz@franz.de>")).toBe ("franz@franz.de");
        });
        test ('should return email address containing multiple wrapped angle brackets', () => {
            expect (StringUtils.extractEmailAddress ("Franz <Ferdinant <franz@franz.de>>")).toBe ("franz@franz.de");
        });
        test ('should work with a plain email address', () => {
            expect (StringUtils.extractEmailAddress ("test@test.com")).toBe ("test@test.com");
        });
        test ('should trim the final string', () => {
            expect (StringUtils.extractEmailAddress (" <test@test.com >")).toBe ("test@test.com");
        });
    });
    describe("findFirstname", () => {
        test('should find first name in Address string', () => {
            expect(StringUtils.findFirstname("Franz Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should find first name in Address string with double name seperated by space', () => {
            expect(StringUtils.findFirstname("Franz Herbert Bauer <franz.bauer@gmail.com>")).toBe("Franz,Herbert");
        });
        test('should find first name in Address string with double name seperated by dash', () => {
            expect(StringUtils.findFirstname("Franz-Herbert Bauer <franz.bauer@gmail.com>")).toBe("Franz,Herbert");
        });
        test('should recognize honorifics', () => {
            expect(StringUtils.findFirstname("Herr Franz Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should recognize female honorifics', () => {
            expect(StringUtils.findFirstname("Frau Franz Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should return empty string if no name found', () => {
            expect(StringUtils.findFirstname("<franz.bauer@gmail.com>")).toBe("franz");
        });
        test('should find first name in Address string', () => {
            expect(StringUtils.findFirstname("Franz_Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should find first name in Address string with dots', () => {
            expect(StringUtils.findFirstname("Franz.Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should recognize title with underscore', () => {
            expect(StringUtils.findFirstname("Herr Franz_Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should recognize name within pure mail address', () => {
            expect(StringUtils.findFirstname("franz.bauer@gmail.com")).toBe("franz");
        });
        test('should recognize name within mail address with underscore', () => {
            expect(StringUtils.findFirstname("franz_bauer@gmail.com")).toBe("franz");
        });
        test('should recognize name within mail address with number', () => {
            expect(StringUtils.findFirstname("franz42@gmail.com")).toBe("franz");
        });
        test('should recognize title with underscore', () => {
            expect(StringUtils.findFirstname("Franz Bauer (franz.bauer@gmail.com) <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should find first name in Address string with quotation marks', () => {
            expect(StringUtils.findFirstname("'Franz Bauer' <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should find first name in reverse order Address string', () => {
            expect(StringUtils.findFirstname("Bauer, Franz Herbert <franz.bauer@gmail.com>")).toBe("Franz,Herbert");
        });
        test('should recognize title', () => {
            expect(StringUtils.findFirstname("Prof. Dr. Franz Bauer <franz.bauer@gmail.com>")).toBe("Franz");
        });
        test('should handle empty string', () => {
            expect(StringUtils.findFirstname("")).toBe("");
        });
        test('should work with full size string', () => {
            expect(StringUtils.findFirstname("Barbara Klein (bklein@fb4.fh-frankfurt.de) <bklein@fb4.fh-frankfurt.de>")).toBe("Barbara");
        });

        test('complicated stuff', () => {
            expect(StringUtils.findFirstname("Ludwig, Hans-Reiner <hrludwig@fb2.fra-uas.de>")).toBe("Hans,Reiner");
        });
        test('another complicated stuff', () => {
            expect(StringUtils.findFirstname("\"Prof. Dr. Jörg Schäfer\" (jschaefer@fb2.fra-uas.de) <jschaefer@fb2.fra-uas.de>")).toBe("Jörg");
        });
    });


    describe("findFullName", () => {
        test('should handle Title', () => {
            expect(StringUtils.findFullName("Prof. Dr. Franz Bauer <franz.bauer@gmail.com>")).toBe("Prof. Dr. Franz Bauer");
        });
        test('should find first name in Address string', () => {
            expect(StringUtils.findFullName("Franz Bauer <franz.bauer@gmail.com>")).toBe("Franz Bauer");
        });
        test('should find first name in Address string with double name seperated by space', () => {
            expect(StringUtils.findFullName("Franz Herbert Bauer <franz.bauer@gmail.com>")).toBe("Franz Herbert Bauer");
        });
        test('should find first name in Address string with double name seperated by dash', () => {
            expect(StringUtils.findFullName("Franz-Herbert Bauer <franz.bauer@gmail.com>")).toBe("Franz-Herbert Bauer");
        });
        test('should recognize honorifics', () => {
            expect(StringUtils.findFullName("Herr Franz Bauer <franz.bauer@gmail.com>")).toBe("Herr Franz Bauer");
        });
        test('should recognize female honorifics', () => {
            expect(StringUtils.findFullName("Frau Franz Bauer <franz.bauer@gmail.com>")).toBe("Frau Franz Bauer");
        });
        test('should return empty string if no name found', () => {
            expect(StringUtils.findFullName("<franz.bauer@gmail.com>")).toBe("franz bauer");
        });
        test('should find first name in Address string', () => {
            expect(StringUtils.findFullName("Franz_Bauer <franz.bauer@gmail.com>")).toBe("Franz Bauer");
        });
        test('should find first name in Address string with dots', () => {
            expect(StringUtils.findFullName("Franz.Bauer <franz.bauer@gmail.com>")).toBe("Franz Bauer");
        });
        test('should recognize title with underscore', () => {
            expect(StringUtils.findFullName("Herr Franz_Bauer <franz.bauer@gmail.com>")).toBe("Herr Franz Bauer");
        });
        test('should recognize name within pure mail address', () => {
            expect(StringUtils.findFullName("franz.bauer@gmail.com")).toBe("franz bauer");
        });
        test('should recognize name within mail address with underscore', () => {
            expect(StringUtils.findFullName("franz_bauer@gmail.com")).toBe("franz bauer");
        });
        test('should recognize name within mail address with number', () => {
            expect(StringUtils.findFullName("franz42@gmail.com")).toBe("franz");
        });
        test('should recognize title with underscore', () => {
            expect(StringUtils.findFullName("Franz Bauer (franz.bauer@gmail.com) <franz.bauer@gmail.com>")).toBe("Franz Bauer");
        });
        test('should find first name in Address string with quotation marks', () => {
            expect(StringUtils.findFullName("'Franz Bauer' <franz.bauer@gmail.com>")).toBe("Franz Bauer");
        });
        test('should find first name in reverse order Address string', () => {
            expect(StringUtils.findFullName("Bauer, Franz Herbert <franz.bauer@gmail.com>")).toBe("Franz Herbert Bauer");
        });
        test('should recognize title', () => {
            expect(StringUtils.findFullName("Prof. Dr. Franz Bauer <franz.bauer@gmail.com>")).toBe("Prof. Dr. Franz Bauer");
        });
        test('should handle empty string', () => {
            expect(StringUtils.findFullName("")).toBe("");
        });
    });


    describe ("find salutation in messagebody", () => {
        let messageBodyOlaf = "Hallo Zusammen,\n" +
            "<br>\n" +
            "wie geht es euch?\n" +
            "<br>\n" +
            "mit freundlichen Grüßen\n" +
            "Olaf";

        let messageBody = "Dear Mr. Franz Bauer,\n" +
            "<br>\n" +
            "<br>\n" +
            "Thank you for the report.\n" +
            "<br>\n" +
            "<br>\n" +
            "Best regards,\n" +
            "<br>Franzi Bauer";

/*
        let messageBodyMultiReciepient = "Dear Mr. Franz Bauer, Mrs. Franzi Mueller,\n" +
            "<br>\n" +
            "<br>\n" +
            "Thank you for the report.\n" +
            "<br>\n" +
            "<br>\n" +
            "Best regards,\n" +
            "<br>Franzi Bauer";
*/

        let messageBodyNoRecipient = "Thank you for the report.\n" +
            "<br>\n" +
            "<br>\n" +
            "Best regards,\n" +
            "<br>Franzi Bauer";

        let salutation = "Dear Mr. Franz Bauer";
        // let salutationMultiRecipients = "Dear Mr. Franz Bauer, Mrs. Franzi Mueller";


        let messageContentOlaf = "wie geht es euch?\n" +
            "<br>\n" +
            "mit freundlichen Grüßen\n" +
            "Olaf";

        let messageContent =
            "Thank you for the report.\n" +
            "<br>\n" +
            "<br>\n" +
            "Best regards,\n" +
            "<br>Franzi Bauer";


        test('should find salutation out of messageBody', () => {
            expect(StringUtils.findSalutation(messageBody)).toBe(salutation);
        });

        /*
        test('should find salutation out of messageBody multi recipient', () => {
            expect(StringUtils.findSalutation(messageBodyMultiReciepient)).toBe(salutationMultiRecipients);
        });

        test('should find no salutation out of messageBody multi recipient', () => {
            expect(StringUtils.findSalutation(messageBodyNoRecipient)).toBe("");
        });

        test('should find content out of messageBody multi recipient', () => {
            expect(StringUtils.findMessageContent(messageBodyMultiReciepient)).toBe(messageContent);
        });
        */

        test('should find content out of messageBody', () => {
            expect(StringUtils.findMessageContent(messageBody)).toBe(messageContent);
        });

        test('should find content out of messageBody without recipients', () => {
            expect(StringUtils.findMessageContent(messageBodyNoRecipient)).toBe(messageContent);
        });

        test('should find content out of messagebodyOlaf', () => {
            expect(StringUtils.findMessageContent(messageBodyOlaf)).toBe(messageContentOlaf);
        });
    })
});
