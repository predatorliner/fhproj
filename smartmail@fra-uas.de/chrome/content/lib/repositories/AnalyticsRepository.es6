import {Repository} from "./Repository";

let regeneratorRuntime = require("chrome://smartmail/content/node_modules/regenerator-runtime/runtime-module.js");

export class AnalyticsRepository extends Repository {

    constructor(dbConfig) {
        super(dbConfig);
    }

    async getAll() {
        let result = await this.execute(`SELECT * FROM Gender_Cache`);
        if (result.length === 0) return null;

        let mappedItems = [];
        for (let i = 0; i < result.length; i++) {
            mappedItems.push(mapNameItem(result[i]));
        }

        return mappedItems
    }

    async delete(name) {
        let query = `DELETE FROM Gender_Cache WHERE Name = \'${name}\';`;
        await this.execute(query);
    }

    async add(nameGender) {
        validate(nameGender);
        let query = `INSERT INTO Gender_Cache (Name, Gender) VALUES (:name, :gender);`;
        await this.execute(query, nameGender);
    }

    async update(nameGender) {
        validate(nameGender);
        let query = `UPDATE Gender_Cache SET Gender = :gender WHERE Name = :name`;
        await this.execute(query, nameGender);
    }

    async getByName(name) {
        let query = `select * from Gender_Cache where Gender_Cache.Name = \'${name}\' LIMIT 1`;
        let result = await this.execute(query);
        if (result.length === 0) return null;
        return mapNameItem(result[0]);
    }
}

let validate = (entity) => {
    if (!entity.name || !entity.gender) {
        throw new Error("name and gender needed for this operation")
    }
};

let mapNameItem = (column) => {
    return {
        name: column.getResultByName("Name"),
        gender: column.getResultByName("Gender")
    }
};