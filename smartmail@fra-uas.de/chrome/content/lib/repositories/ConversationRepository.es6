/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


import {Repository} from "./Repository";

let regeneratorRuntime = require ("chrome://smartmail/content/node_modules/regenerator-runtime/runtime-module.js");

export class ConversationRepository extends Repository {
    constructor (dbConfig) {
        super (dbConfig)
    }

    async getByEmail (email) {
        let result = await this.execute (`SELECT * FROM Conversation WHERE Conversation.Email = lower('${email}') LIMIT 1`);
        if (result.length === 0) {
            return null;
        }
        return mapConversationEntity (result[0]);
    }

    async add (conversation) {
        validate (conversation);
        let query = `INSERT INTO Conversation (Email, Gender, Salutation, Farewell, Languages)
                            VALUES (:email, :gender, :salutation, :farewell, :languages);`;
        await this.execute (query, conversation);
    }

    async update (conversation) {
        validate (conversation);
        let query = `UPDATE Conversation
                        SET Gender = :gender,
                            Salutation = :salutation,
                            Farewell = :farewell,
                            Languages = :languages
                        WHERE
                            Email = :email`;
        await this.execute (query, conversation);
    }
}

let mapConversationEntity = (result) => {
    return {
        email:      result.getResultByName ("Email"),
        gender:     result.getResultByName ("Gender"),
        salutation: result.getResultByName ("Salutation"),
        farewell:   result.getResultByName ("Farewell"),
        languages:  result.getResultByName ("Languages"),
    };
};

let validate = (entity) => {
    if (! entity.email) {
        throw new Error ("email needed for this operation")
    }
};

