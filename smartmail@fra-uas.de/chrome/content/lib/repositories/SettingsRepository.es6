import {Repository} from "./Repository";

let regeneratorRuntime = require("chrome://smartmail/content/node_modules/regenerator-runtime/runtime-module.js");

export class SettingsRepository extends Repository {

    constructor(dbConfig) {
        super(dbConfig);
    }

    /**
     * @returns {Promise<{key, value}>}
     */
    async getIsSalutationEnabled() {
        let result = await this.execute(`SELECT * FROM Settings WHERE key = 'isSalutationEnabled';`);
        if (result.length === 0) throw "Setting isSalutationEnabled not found";
        return mapSetting(result[0]);
    }

    /**
     * @param value - value of IsSalutationEnabled entry to update : "true" or "false"
     */
    async updateIsSalutationEnabled(value) {
        let query = `UPDATE Settings SET value = '${value}' WHERE key = 'isSalutationEnabled';`;
        await this.execute(query);
    }
}

let mapSetting = (column) => {
    return {
        key: column.getResultByName("key"),
        value: column.getResultByName("value"),
    };
};

