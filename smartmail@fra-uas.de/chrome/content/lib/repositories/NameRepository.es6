import {Repository} from "./Repository";

let regeneratorRuntime = require("chrome://smartmail/content/node_modules/regenerator-runtime/runtime-module.js");

export class NameRepository extends Repository {

    constructor(dbConfig) {
        super(dbConfig);
    }

    async getByFirstnameByLanguage(language, firstName) {
        let result = await this.execute(`SELECT * FROM Name 
                                         WHERE Name.Firstname = lower('${firstName}') 
                                         AND Name.Country = lower('${language}')
                                         LIMIT 1`);
        if (result.length === 0) return null;
        return mapNameItem(result[0]);
    }

    async getByFirstName(firstName) {
        let result = await this.execute(`SELECT * FROM Name WHERE Name.Firstname = lower('${firstName}') LIMIT 1`);
        if (result.length === 0) return null;
        return mapNameItem(result[0]);
    }


}

let mapNameItem = (column) => {
    return {
        nameId: column.getResultByName("NameID"),
        firstname: column.getResultByName("Firstname"),
        gender: column.getResultByName("Gender"),
        country: column.getResultByName("Country"),
    };
};

