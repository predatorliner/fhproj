import {FileUtils} from "../utils/FileUtils";

let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

export class Repository {
    constructor(dbConfig) {
        this.connectionString = dbConfig.connectionString;
        this.Services = dbConfig.chrome.Cu.import("resource://gre/modules/Services.jsm").Services;
        this.Task = dbConfig.chrome.Cu.import("resource://gre/modules/Task.jsm").Task;
        this.Sqlite = dbConfig.chrome.Cu.import("resource://gre/modules/Sqlite.jsm", {});
    }

    async open() {
        let file = this.connectionString;
        return await this.Sqlite.openConnection({
            path: file,
            sharedMemoryCache: false
        })
    }

    async execute(statement, params = null, onRow = null) {
        try {
            if (this.dbConnection === undefined) {
                this.dbConnection = await this.open();
            }
            return await this.dbConnection.execute(statement, params, onRow);
        } catch (ex) {
            console.log(`statement: ${statement} caused error: ${ex.toString()}`);
            throw ex;
        } finally {
            await this.close();
        }
    }


    async close() {
        try {
            await this.dbConnection.close();
        } catch (ex) {
            console.log(ex);
            throw ex;
        } finally {
            this.dbConnection = undefined;
        }
    }
}
