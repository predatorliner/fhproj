let {Repository} = require("./Repository");

describe("Repository", () => {

    let imp;
    let dbConfig;
    let chrome;
    beforeEach(() => {

        imp = {
            FileUtils: {},
            Services: {
                io: {
                    newURI: jest.fn()
                },
            },
            openConnection: jest.fn()
        };

        chrome = {
            Cu: {
                import: () => {
                    return imp;
                }
            },
            Cc: {
                '@mozilla.org/chrome/chrome-registry;1': {
                    getService: jest.fn(() => {
                        return {
                            convertChromeURL :  jest.fn(() => {
                                return {
                                    QueryInterface: jest.fn(() => {
                                        return {
                                            file: {path: "dfdf"}
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            },
            Ci: {
                nsIChromeRegistry: {}
            }
        };

        dbConfig={
            connectionString:  "connectionString",
        };

        Object.defineProperty(dbConfig, "chrome", {
            get: ()=>{
                return chrome;
            }
        })

    });

    function CreateRepository() {
        return new Repository(dbConfig);
    }

    test("Repository constructor works", () => {
        let repository = CreateRepository();
    });

    test("open function works", async () => {
        let connection = await CreateRepository().open();
        expect(imp.openConnection).toHaveBeenCalled();
    })
});

