export class HttpClient {
    constructor(chrome, baseUri = "") {
        this.baseUri = baseUri;
        this.Cc = chrome.Cc;
        this.Ci = chrome.Ci;
    }

    get(url) {
        return new Promise((res, rej) => {
            let request = this._createRequest(res, rej);
            request.open("GET", this.baseUri + url, true);
            request.send(null);
        });
    }

    post(url, data) {
        return new Promise((res, rej) => {
            let request = this._createRequest(res, rej);
            request.open("POST", this.baseUri + url, true);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(JSON.stringify(data));
        });
    }

    _createRequest(res, rej) {
        let request = this.Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(this.Ci.nsIXMLHttpRequest);
        request.onload = (aEvent) => {
            let data = aEvent.target.responseText;
            res({
                data: data ? JSON.parse(data) : null,
                status: aEvent.target.status
            });
        };
        request.onerror = (aEvent) => rej(Error(`Http Request Error, Status: ${aEvent.target.status}`));
        request.ontimeout = () => rej(Error('Http Request timed out'));
        return request;
    }
}
