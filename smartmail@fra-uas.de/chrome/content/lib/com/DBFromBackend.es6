//! Babel Bug, that's why I'm using a promise instead of async here: https://github.com/babel/babel/issues/6956
function databaseFileModifiedDateFrom(DB_URL) {
    const {Cu, Cc, Ci} = require("chrome");

    Cu.import("resource://gre/modules/FileUtils.jsm");
    Cu.import("resource://gre/modules/Console.jsm");
    Cu.import("resource://gre/modules/NetUtil.jsm");
    Cu.import('resource://gre/modules/Services.jsm');
    Cu.import("resource://gre/modules/osfile.jsm");
    Cu.import("resource://gre/modules/Promise.jsm");

    return new Promise(function(resolve, reject) {
        let xhr = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);
        xhr.mozBackgroundRequest = true;
        xhr.open('HEAD', DB_URL, true);
        xhr.channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS | Ci.nsIRequest.LOAD_BYPASS_CACHE | Ci.nsIRequest.INHIBIT_PERSISTENT_CACHING;
        xhr.ontimeout = function () {
            reject('timeout')
        };
        xhr.onload = function() { resolve(Date.parse(this.getResponseHeader("Last-Modified"))); };
        xhr.send(null);
    })
}

export function namesDBHashFrom(NAMES_DB_HASH_ENDPOINT_URL) {
    const {Cu, Cc, Ci} = require("chrome");

    Cu.import("resource://gre/modules/FileUtils.jsm");
    Cu.import("resource://gre/modules/Console.jsm");
    Cu.import("resource://gre/modules/NetUtil.jsm");
    Cu.import('resource://gre/modules/Services.jsm');
    Cu.import("resource://gre/modules/osfile.jsm");
    Cu.import("resource://gre/modules/Promise.jsm");

    return new Promise(function (resolve, reject) {
        let xhr = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);
        xhr.mozBackgroundRequest = true;
        xhr.open('GET', NAMES_DB_HASH_ENDPOINT_URL, true);
        xhr.channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS | Ci.nsIRequest.LOAD_BYPASS_CACHE | Ci.nsIRequest.INHIBIT_PERSISTENT_CACHING;
        xhr.ontimeout = function () {
            reject('timeout')
        };
        xhr.onload = function() { resolve(xhr.response); };
        xhr.send(null);
    })
}

export function downloadDBFileFrom(url, to) {
    const {Cu, Cc, Ci} = require("chrome");

    Cu.import("resource://gre/modules/FileUtils.jsm");
    Cu.import("resource://gre/modules/Console.jsm");
    Cu.import("resource://gre/modules/NetUtil.jsm");
    Cu.import('resource://gre/modules/Services.jsm');
    Cu.import("resource://gre/modules/osfile.jsm");
    Cu.import("resource://gre/modules/Promise.jsm");

    xhr(url, data => {
        let promised = OS.File.writeAtomic(to, new Uint8Array(data));
        promised.then(
            function() {
                console.log('Succesfully fetched new database from server.')
            },
            function(ex) {
                console.log(ex)
            }
        );
    });
}

// https://stackoverflow.com/questions/25492225/how-to-download-image-to-desktop-with-os-file
function xhr(url, callback) {
    const {Cu, Cc, Ci} = require("chrome");

    Cu.import("resource://gre/modules/FileUtils.jsm");
    Cu.import("resource://gre/modules/Console.jsm");
    Cu.import("resource://gre/modules/NetUtil.jsm");
    Cu.import('resource://gre/modules/Services.jsm');
    Cu.import("resource://gre/modules/osfile.jsm");
    Cu.import("resource://gre/modules/Promise.jsm");

    let xhr = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);

    let handler = ev => {
        evf(m => xhr.removeEventListener(m, handler, !1));
        switch (ev.type) {
            case 'load':
                if (xhr.status == 200) {
                    callback(xhr.response);
                    break;
                }
            default:
                // Services.prompt.alert(null, 'SmartMail Update Error', 'Could not fetch latest names file from server: ' + xhr.statusText + ' [' + ev.type + ':' + xhr.status + ']');
                break;
        }
    };

    let evf = f => ['load', 'error', 'abort'].forEach(f);
    evf(m => xhr.addEventListener(m, handler, false));

    xhr.mozBackgroundRequest = true;
    xhr.open('GET', url, true);
    xhr.channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS | Ci.nsIRequest.LOAD_BYPASS_CACHE | Ci.nsIRequest.INHIBIT_PERSISTENT_CACHING;
    xhr.responseType = "arraybuffer";
    xhr.send(null);
}