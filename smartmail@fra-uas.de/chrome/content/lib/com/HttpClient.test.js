let {HttpClient} = require("./HttpClient.js");

let createHttpClient = chrome => {
    return new HttpClient(chrome, "https://smartmail-backend.herokuapp.com");
};

describe("HttpClient", () => {
    let chrome;
    let request;
    let onloadEvent = {
        target: {
            responseText: '{"test":1}',
            status: 200
        }
    };

    beforeEach(() => {

        request = {
            open: jest.fn(() => {
                request.onload(onloadEvent);
            }),
            send: jest.fn(),
            setRequestHeader: jest.fn()
        };

        chrome = {
            Cc: {
                '@mozilla.org/xmlextras/xmlhttprequest;1': {
                    createInstance: jest.fn(() => {
                        return request
                    })
                }
            },
            Ci: {
                nsIXMLHttpRequest: {}
            }
        };

    });

    describe("get", () => {
        test("should resolve data and status", async () => {
            let result = await createHttpClient(chrome).get("/names");
            expect(result.data.test).toBe(1);
            expect(result.status).toBe(200)
        });
        test("should call open with concated uri", async () => {
            await createHttpClient(chrome).get("/names");
            expect(request.open).toHaveBeenCalledWith(
                "GET",
                "https://smartmail-backend.herokuapp.com/names",
                true);
        });
        test("should call send with null", async () => {
            await createHttpClient(chrome).get("/names");
            expect(request.send).toHaveBeenCalledWith(null);
        });
    });
    describe("post", () => {
        test("should resolve data and status", async () => {
            let result = await createHttpClient(chrome).post("/names");
            expect(result.data.test).toBe(1);
            expect(result.status).toBe(200)
        });
        test("should call open with concated uri and payload", async () => {
            await createHttpClient(chrome).post("/names");
            expect(request.open).toHaveBeenCalledWith(
                "POST",
                "https://smartmail-backend.herokuapp.com/names",
                true);
        });
        test("should stringify and send given data", async () => {
            await createHttpClient(chrome).post("/names", {payloadData: 1});
            expect(request.send).toHaveBeenCalledWith("{\"payloadData\":1}");
        });
        test("should set correct request header", async () => {
            await createHttpClient(chrome).post("/names", {payloadData: 1});
            expect(request.setRequestHeader).toHaveBeenCalledWith("Content-Type", "application/x-www-form-urlencoded");
        });
    });
});

