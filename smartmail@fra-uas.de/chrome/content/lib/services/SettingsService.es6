let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

export class SettingsService {
    constructor(settingsRepository) {
        this.settingsRepository = settingsRepository;
    }

    async IsSalutationEnabled() {
        return ((await this.settingsRepository.getIsSalutationEnabled()).value) === "true";
    }

    async updateIsSalutationEnabled(value) {
        await this.settingsRepository.updateIsSalutationEnabled(value);
    }
}

