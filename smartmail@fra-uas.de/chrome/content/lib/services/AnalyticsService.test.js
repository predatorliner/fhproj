let {AnalyticsService} = require("./AnalyticsService.js");

describe("AnalyticsService", () => {

    let analyticsRepository = {
        update: jest.fn(),
        add: jest.fn(),
        delete: jest.fn(),
        getAll: jest.fn(() => {
            return [{name: "name", gender: "m"}]
        }),
        getByName: jest.fn()
    };

    let httpClient = {
        post: jest.fn(),
        get: jest.fn(),
    };

    afterEach(() => {
        jest.resetAllMocks()
    });

    let getAnalyticsService = () => new AnalyticsService(analyticsRepository, httpClient);

    describe("postToServer", () => {
        test("should get all nameGender items", async () => {
            httpClient.post = jest.fn(() => {return {status: 200, data: null}});
            await getAnalyticsService().postToServer();
            expect(analyticsRepository.getAll).toHaveBeenCalled()
        });
        test("should delete all posted items", async () => {
            httpClient.post = jest.fn(() => {return {status: 202, data: null}});
            analyticsRepository.getAll = () => {
                return [{name: "name", gender: "m"}]
            };
            await getAnalyticsService().postToServer();
            expect(analyticsRepository.delete).toHaveBeenCalledWith("name")
        });
        test("should not delete all posted items if status is not 200", async () => {
            httpClient.post = jest.fn(() => {return {status: 400, data: null}});
            analyticsRepository.getAll = () => {
                return [{name: "name", gender: "m"}]
            };
            await getAnalyticsService().postToServer();
            expect(analyticsRepository.delete).not.toHaveBeenCalledWith("name")
        });
        test("should post data to server", async () => {
            httpClient.post = jest.fn(() => {return {status: 200, data: null}});
            let names = [{name: "name", gender: "m"}];
            analyticsRepository.getAll = () => {
                return names;
            };
            await getAnalyticsService().postToServer();
            expect(httpClient.post).toHaveBeenCalledWith("/names", names);
        });
        test("should handle empty array", async () => {
            let names = [];
            analyticsRepository.getAll = () => {
                return names;
            };
            await getAnalyticsService().postToServer();
            expect(httpClient.post).not.toHaveBeenCalled();
            expect(analyticsRepository.delete).not.toHaveBeenCalled();
        });
        test("should handle null", async () => {
            let names = null;
            analyticsRepository.getAll = () => {
                return names;
            };
            await getAnalyticsService().postToServer();
            expect(httpClient.post).not.toHaveBeenCalled();
            expect(analyticsRepository.delete).not.toHaveBeenCalled();
        });
    });
    describe("addOrUpdateGender", () => {
        test("should add Gender_Cache entry", async () => {
            let name = "Peter";
            let gender = "m";

            analyticsRepository.getByName = jest.fn(() => {return null});
            await getAnalyticsService().addOrUpdateGender(name, gender);
            expect(analyticsRepository.add).toHaveBeenCalledWith({name: name, gender: gender});
        });
        test("should update Gender_Cache entry", async () => {
            let name = "Peter";
            let gender = "f";
            analyticsRepository.getByName = jest.fn(() => {
                return {name: "Peter", gender: "f"}
            });
            let AnalyticsService = getAnalyticsService();
            await AnalyticsService.addOrUpdateGender(name, gender);
            expect(analyticsRepository.update).toHaveBeenCalledWith({name: "Peter", gender: "f"});
        })
        test("should not add a not female or male person", async () => {
            let name = "Peter";
            let gender = "g";

            analyticsRepository.getByName = jest.fn(() => {return null});
            await getAnalyticsService().addOrUpdateGender(name, gender);
            !expect(analyticsRepository.add).not.toHaveBeenCalled();
        })
    })
});

