let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");
import {CollUtils} from "../utils/CollUtils";

export class AnalyticsService {
    constructor(analyticsRepository, httpClient) {
        this.analyticsRepository = analyticsRepository;
        this.httpClient = httpClient;
    }

    async postToServer() {
        let nameItems = await this.analyticsRepository.getAll();
        if (!CollUtils.isNullOrEmpty(nameItems)) {
            let result = await this.httpClient.post("/names", nameItems);
            if (result.status === 202) {
                nameItems.forEach(async i => await this.analyticsRepository.delete(i.name))
            }
        }
    }


    async addOrUpdateGender(name, gender) {
        if (isNotMaleOrFemale(gender)) {
            return
        }

        let nameGender = {name: name, gender: gender};
        let nameItem = await this.analyticsRepository.getByName(name);
        if (!nameItem) {
            await this.analyticsRepository.add(nameGender);
        }
        else {
            await this.analyticsRepository.update(nameGender);
        }
    }
}

let isNotMaleOrFemale = (gender) => gender !== "m" && gender !== "f";