let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");
import {downloadDBFileFrom, namesDBHashFrom} from "../com/DBFromBackend";
import {SHA256HashFor} from "../utils/FileUtils";

export class UpdateService {
    constructor(genericRepo) {
        this.genericRepo = genericRepo;
    }

    /**
     * Call this method to initiate an update for the names database.
     * @param targetFilePath Path where the db file from the server should be stored.
     */
    checkForNamesDBUpdates(targetFilePath) {
        const {Cu} = require("chrome");

        Cu.import("resource://gre/modules/Services.jsm");
        Cu.import("resource://gre/modules/Console.jsm");
        Cu.import("resource://gre/modules/osfile.jsm");
        Cu.import("resource://gre/modules/FileUtils.jsm");
        Cu.import("resource://gre/modules/Promise.jsm");

        const URL = "https://smartmail-backend.herokuapp.com/db";

        OS.File.exists(targetFilePath).then((fileExists) => {
            if (!fileExists) {
                downloadDBFileFrom(URL, targetFilePath);
            }

            if (fileExists) {
                this.genericRepo.execute(`SELECT * FROM Settings WHERE key = 'lastCheckForNamesDBUpdates';`)
                    .then(result => {
                        let lastCheckedUnixTimestamp = parseInt(result[0].getResultByName("value"));
                        if (!Number.isInteger(lastCheckedUnixTimestamp)) {
                            lastCheckedUnixTimestamp = 0;
                        }

                        if ((Date.now() - lastCheckedUnixTimestamp) > (1000 * 60)) {
                            console.log("Checking for names db update...");
                            namesDBHashFrom(URL + "/hash")
                                .then(hash => {
                                    if (hash !== SHA256HashFor(targetFilePath)) {
                                        console.log("Local and server out of sync. Updating...");
                                        downloadDBFileFrom(URL, targetFilePath);
                                    }
                                    this.genericRepo.execute(`UPDATE Settings SET value = '${Date.now()}' WHERE key = 'lastCheckForNamesDBUpdates';`);
                                })
                                .catch(reason => console.log(reason));
                        } else {
                            console.log("Recently checked for names db update. Skipping...")
                        }
                    })
                    .catch(reason => {
                        console.log(reason)
                    });
            }
        });
    }
}

