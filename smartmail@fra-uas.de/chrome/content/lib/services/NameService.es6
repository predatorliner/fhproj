let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

export class NameService {
    constructor(nameRepository) {
        this.nameRepository = nameRepository;
    }

    async getGenderByFirstName(firstName) {
        let gender = await this.nameRepository.getByFirstName(firstName);
        return gender === null ? null : gender.gender;
    }
}

