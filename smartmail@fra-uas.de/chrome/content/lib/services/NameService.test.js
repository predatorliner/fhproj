let {NameService} = require("./NameService.js");
let {StringUtils} = require("../utils/StringUtils");

describe("NameService", () => {
    let nameRepository = {getByFirstName: jest.fn()};

    afterEach(() => {
        jest.resetAllMocks()
    });

    let getNameService = () => new NameService(nameRepository);


    describe("getGenderByFirstName", () => {
        test("should return by firstName", async () => {
            let recipientString = "FirstName";

            nameRepository.getByFirstName.mockImplementation(async (email) => {
                return {
                    id: 1,
                    firstName: "FirstName",
                    gender: "m",
                    country: "de"
                }
            });

            let result = await getNameService().getGenderByFirstName(recipientString);

            expect(result).toBe("m")
        });
        test("should return null when repo returns null", async () => {
            let recipientString = "FirstName";

            nameRepository.getByFirstName.mockImplementation(async (email) => {
                return null
            });

            let result = await getNameService().getGenderByFirstName(recipientString);

            expect(result).toBe(null)
        });
    });
});

