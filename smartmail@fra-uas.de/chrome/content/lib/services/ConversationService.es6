/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


let regeneratorRuntime = require ("../../node_modules/regenerator-runtime/runtime-module.js");
import {CollUtils} from "../utils/CollUtils";

export class ConversationService {
    constructor (conversationRepository) {
        this.conversationRepository = conversationRepository;
    }

    async addOrUpdateSalutation (email, salutation) {
        let conversation = await this.conversationRepository.getByEmail (email);
        if (conversation) {
            conversation.salutation = salutation;
            await this.conversationRepository.update (conversation)
        } else {
            await this.conversationRepository.add ({
                email: email,
                salutation: salutation
            })
        }
    }

    async addOrUpdateGender (email, gender) {
        let conversation = await this.conversationRepository.getByEmail (email);
        if (conversation) {
            conversation.gender = gender;
            await this.conversationRepository.update (conversation)
        } else {
            await this.conversationRepository.add ({
                email: email,
                gender: gender
            })
        }
    }

    async getGenderByEmail (email) {
        let conversation = await this.conversationRepository.getByEmail (email);
        return conversation ? conversation.gender : undefined;
    }

    async getSalutationByEmail (email) {
        let conversation = await this.conversationRepository.getByEmail (email);
        return conversation ? conversation.salutation : undefined;
    }

    async getPreferredLanguageByEmail (email) {
        let conversation = await this.conversationRepository.getByEmail (email);
        let languages = conversation ? conversation.languages : undefined;
        if (! languages) {
            return undefined;
        } else {
            languages = JSON.parse (languages);
            let preferredLanguage = {count: 0};
            languages.forEach (function (element) {
                if (element.count > preferredLanguage.count) {
                    preferredLanguage = element
                }
            });
            return preferredLanguage.lang;
        }
    }

    async addOrUpdateLanguages (email, language) {
        let conversation = await this.conversationRepository.getByEmail (email);
        if (! conversation) {
            conversation = {
                email: email,
                languages: []
            };
            await this.conversationRepository.add (conversation);
        }
        if (! CollUtils.isNullOrEmpty (conversation.languages)) {
            let languages = JSON.parse (conversation.languages);
            if (languages.filter (i => language === i.lang).length === 0) {
                let obj = {};
                obj["lang"] = language;
                obj["count"] = 1;
                languages.push (obj);
            } else {
                languages.filter (i => language === i.lang).map (i => {
                    i["count"] += 1;
                    return i;
                });
            }
            conversation.languages = JSON.stringify (languages);
            await this.conversationRepository.update (conversation);
        } else {
            conversation.languages = JSON.stringify ([{lang: language, count: 1}]);
            await this.conversationRepository.update (conversation);
        }
    }
}