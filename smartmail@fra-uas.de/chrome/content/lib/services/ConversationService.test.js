/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


let {ConversationService} = require ("./ConversationService.js");

describe("ConversationService", () => {
    let conversationRepository = {getByEmail: jest.fn(), update: jest.fn(), add: jest.fn(),};

    afterEach(() => {
        jest.resetAllMocks()
    });

    let getConversationService = () => new ConversationService(conversationRepository);

    describe("addOrUpdateSalutation", () => {
        test("should update conversation", async () => {
            let email = "recipient@email.com";
            let newSalutation = "new salutation";
            conversationRepository.getByEmail.mockImplementation(async (email) => {
                return {
                    email: email,
                    salutation: "old salutation"
                }
            });

            await getConversationService().addOrUpdateSalutation(email, newSalutation);

            expect(conversationRepository.update).toHaveBeenCalledWith({email: email, salutation: newSalutation})
        });

        test("should add conversation if not exists", async () => {
            let email = "recipient@email.com";
            let newSalutation = "new salutation";

            await getConversationService().addOrUpdateSalutation(email, newSalutation);

            expect(conversationRepository.add).toHaveBeenCalledWith({email: email, salutation: newSalutation})
        })
    });

    describe("addOrUpdateGender", () => {
        test("should update conversation", async () => {
            let email = "recipient@email.com";
            let newGender = "new gender";
            conversationRepository.getByEmail.mockImplementation(async (email) => {
                return {
                    email: email,
                    gender: "old gender"
                }
            });

            await getConversationService().addOrUpdateGender(email, newGender);

            expect(conversationRepository.update).toHaveBeenCalledWith({email: email, gender: newGender})
        });

        test("should add conversation if not exists", async () => {
            let email = "recipient@email.com";
            let newGender = "new gender";

            await getConversationService().addOrUpdateGender(email, newGender);

            expect(conversationRepository.add).toHaveBeenCalledWith({email: email, gender: newGender})
        })
    });

    describe("getGenderByEmail", () => {
        test("should return undefined if not found", async () => {
            let email = "recipient@email.com";
            conversationRepository.getByEmail.mockImplementation(async (email) => undefined);
            let result = await getConversationService().getGenderByEmail(email);
            expect(result).toBe(undefined)
        });
        test("should return gender by email", async () => {
            let email = "recipient@email.com";
            let conversation = {
                email: email,
                gender: "gender"
            };
            conversationRepository.getByEmail.mockImplementation(async (email) => conversation);

            let result = await getConversationService().getGenderByEmail(email);

            expect(result).toBe(conversation.gender)
        });
    });

    describe("getSalutationByEmail", () => {
        test("should return undefined if not found", async () => {
            let email = "recipient@email.com";
            conversationRepository.getByEmail.mockImplementation(async (email) => undefined);
            let result = await getConversationService().getSalutationByEmail(email);
            expect(result).toBe(undefined)
        });
        test("should return salutation by email", async () => {
            let email = "recipient@email.com";
            let conversation = {
                email: email,
                salutation: "salutation"
            };
            conversationRepository.getByEmail.mockImplementation(async (email) => conversation);

            let result = await getConversationService().getSalutationByEmail(email);

            expect(result).toBe(conversation.salutation)
        });
    });

    describe("addOrUpdateLanguages", () => {
        let email = "recipient@email.com";

        test ("should call update on increment language", async () => {
            let conversation = {
                email: email,
                languages: '[{"lang": "de-DE", "count": 30},{"lang": "en-US", "count": 40}]'
            };
            conversationRepository.getByEmail.mockImplementation (async (email) => conversation);

            await getConversationService ().addOrUpdateLanguages (email, "de-DE");

            expect (conversationRepository.update).toHaveBeenCalled ();
        });
        test ("case1: languages im null", async () => {
            let conversation = {email: email, languages: null};
            conversationRepository.getByEmail.mockImplementation (async (email) => conversation);

            await getConversationService ().addOrUpdateLanguages (email, "de-DE");

            expect (conversationRepository.update).toBeCalledWith (
                {email: email, languages: "[{\"lang\":\"de-DE\",\"count\":1}]"});
        });
        test ("case2: languages not null and de-DE not exists", async () => {
            let conversation = {email: email, languages: '[{"lang": "en-US", "count": 40}]'};
            conversationRepository.getByEmail.mockImplementation (async (email) => conversation);

            await getConversationService ().addOrUpdateLanguages (email, "de-DE");

            expect (conversationRepository.update).toBeCalledWith ({
                email: email,
                languages: "[{\"lang\":\"en-US\",\"count\":40},{\"lang\":\"de-DE\",\"count\":1}]"
            });
        });
        test ("case3: languages not null and de-DE exists", async () => {
            let conversation = {
                email: email,
                languages: '[{"lang": "en-US", "count": 40},{"lang": "de-DE", "count": 1}]'
            };
            conversationRepository.getByEmail.mockImplementation (async (email) => conversation);

            await getConversationService ().addOrUpdateLanguages (email, "de-DE");

            expect (conversationRepository.update).toBeCalledWith ({
                email: email,
                languages: "[{\"lang\":\"en-US\",\"count\":40},{\"lang\":\"de-DE\",\"count\":2}]"
            });
        });
        test ("conversation is null", async () => {
            let conversation = null;
            conversationRepository.getByEmail.mockImplementation (async (email) => conversation);

            await getConversationService ().addOrUpdateLanguages (email, "de-DE");

            expect (conversationRepository.update).toBeCalledWith (
                {email: email, languages: "[{\"lang\":\"de-DE\",\"count\":1}]"});
        });
    });
});

