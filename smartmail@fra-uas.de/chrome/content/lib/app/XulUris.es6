export let XulUris = {
    composeMessageURLs: ["chrome://messenger/content/messengercompose/messengercompose.xul"],
    addressBookURLs: ["chrome://messenger/content/addressbook/abNewCardDialog.xul", // New contact
        "chrome://messenger/content/addressbook/abEditCardDialog.xul" // Edit contact
    ]
};