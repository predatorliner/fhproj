let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

import {StringUtils} from "../utils/StringUtils";

export class GenderFromServiceOrNameController {

    constructor(conversationService, nameService) {
        this.conversationService = conversationService;
        this.nameService = nameService;
    }

    async getGenderFromConversationServiceOrNameService(recipientString) {
        let email = StringUtils.extractEmailAddress(recipientString);
        let firstName = StringUtils.findFirstname(recipientString);

        let isGuessed = false;
        let gender = await this.conversationService.getGenderByEmail(email);
        if (!gender) {
            gender = await this.nameService.getGenderByFirstName(firstName);
            isGuessed = true;
        }

        if (gender === null) {
            gender = 'n';
        }

        return {gender: gender, isGuessed: isGuessed};
    }
}