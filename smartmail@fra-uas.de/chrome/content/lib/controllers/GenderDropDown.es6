let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

export class GenderDropDown {
    // this.doc, "addressBook",gender, "64","default",(selectedGender)
    //TODO refactor constructor
    constructor(document, id, originGender = "m", size = "16", callback) {
        this.doc = document;
        this.callback = callback;
        this.id = id;
        this.size = size;

        this.genders = new Map();

        this.genders.set('m', "male");
        this.genders.set('f', "female");
        this.genders.set('g', "group");
        // this.genders.set('u', "unisex");
        this.genders.set('n', "unclear");

        this.menuList = this.doc.createElement("toolbarbutton");
        this.menuList.setAttribute("id", "genderDropdown" + this.id);
        this.menuList.setAttribute("type", "menu");
        this.updateGender(originGender);
        this.menuList.setAttribute("sizetopopup", "none");

        this.menuList.addEventListener("command", (e) => {
            this.menuList.image = this._getGenderImage(e.target.gender);
            this.callback(e.target.gender);
        });

        let menuPopup = this.doc.createElement("menupopup");


        for (let [key, value] of this.getGenders().entries()) {

            let menuItem = this.doc.createElement("menuitem");
            menuItem.id = key + id;
            menuItem.gender = key;
            menuItem.setAttribute("image", this._getGenderImage(key));
            menuItem.setAttribute("label", value);
            menuItem.classList.add("genderElement");
            menuItem.classList.add("menuitem-iconic");
            menuPopup.appendChild(menuItem);
        }

        this.menuList.appendChild(menuPopup);
    }

    updateGender(gender) {
        // todo uncomment when guessed images are available
        console.log("Gender: " + gender.gender + " isGuessed: " + gender.isGuessed);
        this.menuList.setAttribute("image", this._getGenderImage(gender.gender, gender.isGuessed));
        this.menuList.gender = gender;
    }

    getDomElement() {
        return this.menuList;
    }

    getGender() {
        return this.menuList.gender;
    }

    getDomId() {
        return this.menuList.id;
    }


    getGenders() {
        return this.genders;
    }


    insertAfterElement(previousSibling) {
        previousSibling.append(this.menuList);

    }

    insertBeforeElement(sibling) {
        sibling.parentElement.insertBefore(this.menuList, sibling);
    }

    addAsChild(parent) {
        parent.appendChild(this.menuList);
    }

    _getGenderImage(genderKey, isGuessed) {
        let url = "chrome://smartmail/content/images/";

        let imageName = this.genders.get(genderKey);

        if (imageName == null) {
            imageName = this.genders.get("n");
        }

        url += imageName + this.size + (isGuessed ? "_isGuessed" : "") + ".png";
        return url;
    }
}
