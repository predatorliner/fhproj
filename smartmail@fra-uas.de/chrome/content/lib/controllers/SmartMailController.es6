/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


let regeneratorRuntime = require ("../../node_modules/regenerator-runtime/runtime-module.js");
import {StringUtils} from "../utils/StringUtils";
import {XulUris} from "../app/XulUris";

export class SmartMailController {
    constructor (conversationService,
                 nameService,
                 analyticsService,
                 settingsService,
                 addressBookDialogController,
                 genderFromServiceOrNameController,
                 genderButtonFactory)
    {
        this.settingsService = settingsService;
        this.analyticsService = analyticsService;
        this.conversationService = conversationService;
        this.nameService = nameService;
        this.addressBookDialogController = addressBookDialogController; // TODO only inject services, no controllers from the same layer
        this.genderFromServiceOrNameController = genderFromServiceOrNameController; // TODO only inject services, no controllers from the same layer
        this.genderButtonFactory = genderButtonFactory;
    }

    async startup (doc) {
        this.doc = doc;
        if (XulUris.addressBookURLs.includes (doc.URL)) { // Address book new/edit contact
            await this.addressBookDialogController.setupAddressBookDialog (doc);
        } else if (XulUris.composeMessageURLs.includes (doc.URL)) { // Compose message window
            this.messageBodyWrapper = await this.genderButtonFactory.createMessageBodyWrapper (doc, this.conversationService, this.settingsService);
            let messageComposingModule = doc.getElementById ("MsgHeadersToolbar");
            messageComposingModule.addEventListener ("change", () => this.onMsgHeadersToolbarChange (), false);
            doc.addEventListener ("compose-send-message", (e) => this.onEmailSent (e), true);

            await this.messageBodyWrapper.createSalutationCheckBox ();
            await this.onMsgHeadersToolbarChange ();
            await this.messageBodyWrapper.startAddSalutationToMessageBody ();

            /*
            let editor = this.doc.getElementById ("content-frame");
            editor.addEventListener ("click", async () => {
                await this.messageBodyWrapper.startAddSalutationToMessageBody ();
            });
            */
        }
    }

    /**
     * Event fired when a recipient has changed
     * @returns {Promise<void>}
     */
    async onMsgHeadersToolbarChange () {
        console.log ("************ onMsgHeadersToolbarChange");
        let recipientFields = this.doc.getElementsByClassName ("textbox-addressingWidget");

        for (let i = 0; i < recipientFields.length; i++) {
            let recipient = recipientFields[i];
            recipient.addEventListener ("blur", async () => {
                console.log ("**------------ blur");
                await this.messageBodyWrapper.startAddSalutationToMessageBody ();
            }, true);

            await this._updateGenderButton (recipient);
        }

        let email = this.getEmailFromFirstRecipient ();

        let preferredLanguage = await this.conversationService.getPreferredLanguageByEmail (email);
        if (preferredLanguage != null) {
            this.doc.documentElement.setAttribute ("lang", preferredLanguage);
        } else {
            this.doc.documentElement.setAttribute ("lang", "en-US");
        }
        await this.messageBodyWrapper.startAddSalutationToMessageBody ();
    }

    async _updateGenderButton (recipientField) {
        let gender = await this.genderFromServiceOrNameController.getGenderFromConversationServiceOrNameService (recipientField.value);
        let genderButton = recipientField.previousSibling;

        if (genderButton !== null) {
            recipientField.parentElement.removeChild (genderButton);
        }

        if (recipientField.value !== "") {
            genderButton = this.genderButtonFactory.createNewGenderButton (this.doc, gender, "16", "genderButton" + recipientField.id.substr (10), async (selectedGender) => {
                let recipient = recipientField.value;
                if (recipient.trim () === "") { // If no recipient was specified, don't create a gender button
                    return;
                }

                let email = StringUtils.extractEmailAddress (recipient);
                let firstName = StringUtils.findFirstname (recipient);
                await this.conversationService.addOrUpdateGender (email, selectedGender);
                await this.analyticsService.addOrUpdateGender (firstName, selectedGender);

            });

            genderButton.insertBeforeElement (recipientField);
        }
        this._removeGenderImageFromCurrentRecipient (recipientField);
    }

    /**
     * Remove the unwanted extra image starting on the third recipient row
     */
    //TODO is this still neede with new dropdown syntax ?
    _removeGenderImageFromCurrentRecipient (recipientField) {
        let currListCell = recipientField.parentNode;
        let currListItem = currListCell.parentNode;
        let listBoxBody = currListItem.parentNode;
        let itemList = listBoxBody.childNodes;

        let isImageItem = (o) => o.nodeName === "textbox" && o.value === "";
        let isLastImage = (o) => o.previousSibling !== null && o.previousSibling.nodeName === "image";

        Array.from (itemList).forEach (i =>
            Array.from (i.childNodes).forEach (cell =>
                Array.from (cell.childNodes)
                    .filter (isImageItem)
                    .filter (isLastImage)
                    .forEach (obj => cell.removeChild (obj.previousSibling))));
    }

    async onEmailSent () {
        let email = this.getEmailFromFirstRecipient ();
        let addressRowsIncludingTo = [];
        let allAddressRows = this.doc.getElementsByClassName ("addressingWidgetItem");
        for (let i = 1; i <= allAddressRows.length; i++) {
            let addressColOptionValue = this.doc.getElementById ("addressCol1#" + i).value;
            let addressColEmail = this.doc.getElementById ("addressCol2#" + i).value;
            if (addressColOptionValue === "addr_to" && addressColEmail !== "") {
                addressRowsIncludingTo.push (i);
            }
        }
        if (addressRowsIncludingTo.length < 2) {
            let salutation = this.messageBodyWrapper.getSalutationFromMessageBody ();
            let email = this.getEmailFromFirstRecipient ();
            await this.conversationService.addOrUpdateSalutation (email, salutation);
        }

        await this.analyticsService.postToServer ();
        let spellcheckLanguage = this.doc.documentElement.getAttribute ("lang");
        await this.conversationService.addOrUpdateLanguages (email, spellcheckLanguage);
    }

//TODO find solution to store salutation for multiple recipients
    getEmailFromFirstRecipient () {
        let firstRecipient = this.doc.getElementById ("addressCol2#1"); //only use first recipient
        return StringUtils.extractEmailAddress (firstRecipient.value);
    }
}