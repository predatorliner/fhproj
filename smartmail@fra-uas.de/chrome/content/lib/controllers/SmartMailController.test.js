let {SmartMailController} = require("./SmartMailController.js");

describe("SmartMailController", () => {
    let genderService;
    let conversationService;
    let nameService;
    let event;
    let document;
    let string;
    afterEach(() => jest.resetAllMocks());
    beforeEach(() => {

        string = {
            indexOf: jest.fn(),

        };

        nameService = {
            getGenderByFirstName: () => {
                return 'm'
            },
        };


        genderService = {
            getGender: jest.fn(),
            updateGenderButton: jest.fn(),
            setGenderImage: jest.fn(),
        };
        conversationService = {
            addOrUpdateSalutation: jest.fn(),
            getGenderByEmail: jest.fn(),

        };

        document = {
            contentDocument: this
        };


        event = {
            target: {
                parentNode: null,
                value: "Franz-Herbert Bauer <franz.bauer@gmail.com>",
                previousSibling: {
                    gender: {},
                    setAttribute: jest.fn()
                },
                getElementById: jest.fn(),
            }
        };

    });


    let GetSmartMailController = () => new SmartMailController(genderService, conversationService, nameService,setTimeout);

    test("SmartMailController loads", () => {
        GetSmartMailController()
    });

});

