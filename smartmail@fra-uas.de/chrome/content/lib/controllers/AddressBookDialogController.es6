import {StringUtils} from "../utils/StringUtils";

let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");

export class AddressBookDialogController {
    constructor(genderFromServiceOrNameController, genderbuttonFactory, conversationService, analyticsService) {
        this.genderFromServiceOrNameController = genderFromServiceOrNameController;
        this.genderButtonFactory = genderbuttonFactory;
        this.conversationService = conversationService;
        this.analyticsService = analyticsService;

    }

    /**
     * Add a gender button to the address book entry dialog
     * @param doc
     * @returns {Promise<void>}
     */
    //TODO refactor setupAddressBookDialog
    async setupAddressBookDialog(doc) {
        let genderContainer = doc.getElementById("GenderContainer");
        if (genderContainer !== null) {
            return;
        }

        let rowMargin = 5;
        let rowHeight = 64 + 2 * rowMargin;

        let hbox = doc.createElement("hbox");
        hbox.setAttribute("id", "GenderContainer");
        hbox.setAttribute("align", "start");
        hbox.setAttribute("height", rowHeight.toString());

        let spacerLeft = doc.createElement("spacer");
        spacerLeft.setAttribute("flex", "1");
        hbox.appendChild(spacerLeft);

        let vboxLabel = doc.createElement("vbox");
        vboxLabel.setAttribute("id", "GenderContainerInnerV");
        vboxLabel.setAttribute("pack", "center");
        vboxLabel.setAttribute("height", rowHeight.toString());
        hbox.appendChild(vboxLabel);

        let label = doc.createElement("label");
        label.setAttribute("control", "Gender");
        label.setAttribute("value", "Gender:");
        vboxLabel.appendChild(label);

        let spacerRight = doc.createElement("spacer");
        spacerRight.setAttribute("flex", "1");
        hbox.appendChild(spacerRight);

        let displayName = doc.getElementById("DisplayName");
        let primaryEmail = doc.getElementById("PrimaryEmail");

        let name = displayName.value;
        let email = primaryEmail.value;

        let nameString = name + " <" + email + ">";

        console.log("nameString: " + nameString);

        let hboxInner = doc.createElement("hbox");
        hboxInner.setAttribute("id", "GenderContainerButtonContainerH");
        hboxInner.setAttribute("pack", "center");
        hboxInner.setAttribute("class", "PhoneEditWidth");
        hboxInner.setAttribute("height", rowHeight.toString());

        let vboxGenderButton = doc.createElement("vbox");
        vboxGenderButton.setAttribute("id", "GenderContainerButtonContainerV");
        vboxGenderButton.setAttribute("pack", "center");

        let gender = await this.genderFromServiceOrNameController.getGenderFromConversationServiceOrNameService(nameString);

        let genderButton = this.genderButtonFactory.createNewGenderButton(doc, gender, "64", "addressBook", async (selectedGender) => {

            let email = StringUtils.extractEmailAddress(nameString);
            if (email !== ""){
                let firstName = StringUtils.findFirstname(nameString);
                await this.conversationService.addOrUpdateGender(email, selectedGender);
                await this.analyticsService.addOrUpdateGender(firstName, selectedGender);
            }

        });
        genderButton.addAsChild(vboxGenderButton);
        hboxInner.appendChild(vboxGenderButton);
        hbox.appendChild(hboxInner);

        let mobile = doc.getElementById("CellularNumberContainer");
        mobile.parentNode.appendChild(hbox);
    }
}