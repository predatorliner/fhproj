let regeneratorRuntime = require("../../node_modules/regenerator-runtime/runtime-module.js");
import {MessageBodyWrapper} from "./MessageBodyWrapper";
import {GenderDropDown} from "./GenderDropDown";

export class GenderButtonFactory{
    createNewGenderButton(doc, gender, size, id, callback) {
        // document, id,originGender="m",size ="16", callback
        return new GenderDropDown(doc, id, gender, size, callback);
    }

    async createMessageBodyWrapper(doc, conversationService, settingsService){
        return new MessageBodyWrapper(doc, conversationService, settingsService);
    }
}