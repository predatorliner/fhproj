/**
 *        .-._
 *      .-| | |
 *    _ | | | |__FRANKFURT
 *  ((__| | | | UNIVERSITY
 *     OF APPLIED SCIENCES
 *
 *  (c) 2016-2018
 */


import {StringUtils} from "../utils/StringUtils";

let regeneratorRuntime = require ("../../node_modules/regenerator-runtime/runtime-module.js");

export class MessageBodyWrapper {
    constructor (doc, conversationService, settingsService) {
        this.doc = doc;
        this.conversationService = conversationService;
        this.settingsService = settingsService;
    }


    getSalutationFromMessageBody () {
        let messageBody = this.getMessageBody ();
        return StringUtils.findSalutation (messageBody.innerHTML);
    }


    getMessageBody () {
        let editor = this.doc.getElementById ("content-frame");
        let editorDocument = editor.contentDocument;
        return editorDocument.getElementsByTagName ("body")[0];
    }


    async startAddSalutationToMessageBody () {
        this.enableSalutation = await this.settingsService.IsSalutationEnabled ();
        if (! this.enableSalutation) {
            return;
        }
        await this.filterAddressRows ();
    }


    /**
     * Filter for all rows that are "TO"
     * If rows >3 then send row number to addSalutationToMessageBody()
     */
    async filterAddressRows () {
        let addressRowsTo = [];
        let allAddressRows = this.doc.getElementsByClassName ("addressingWidgetItem");
        for (let i = 1; i <= allAddressRows.length; i++) {
            let addressColOptionValue = this.doc.getElementById ("addressCol1#" + i).value;
            let addressColEmail = this.doc.getElementById ("addressCol2#" + i).value;
            if (addressColOptionValue === "addr_to" && addressColEmail !== "") {
                addressRowsTo.push (i);
            }
        }
        if (addressRowsTo.length === 1) {
            addressRowsTo[1] = "";
            await this.addSalutationToMessageBody (addressRowsTo[0], addressRowsTo[1]);
        //} else if (addressRowsTo.length <= 2) {
        //    await this.addSalutationToMessageBody (addressRowsTo[0], addressRowsTo[1]);
        //} else {
        //    let addressRowsTo = [];
        //    await this.addSalutationToMessageBody (addressRowsTo[0], addressRowsTo[1]);
        }
    }


    /**
     * Get email and if available the salutation from the DB
     * If one email exists then run addSalutationOneRecipient()
     * If two emails exist then run addSalutationTwoRecipient()
     */
    async addSalutationToMessageBody (addressRowsTo0, addressRowsTo1) {
        let email = this.getEmailFromRecipient (addressRowsTo0);
        let secondEmail = this.getEmailFromRecipient (addressRowsTo1);
        let messageBody = this.getMessageBody ();
        // console.log ("***........... messageBody", messageBody);
        let messageBodyNew = StringUtils.findMessageContent (messageBody.innerHTML);
        // console.log ("***........... messageBodyNew", messageBodyNew);
        let salutation = await this.conversationService.getSalutationByEmail (email);
        let secondSalutation = await this.conversationService.getSalutationByEmail (secondEmail);
        let genderEmail = await this.conversationService.getGenderByEmail (email);
        let genderSecondEmail = await this.conversationService.getGenderByEmail (secondEmail);

        if (! StringUtils.isEmptyOrWhiteSpace (salutation) && StringUtils.isEmptyOrWhiteSpace (secondSalutation)) {
            await this.addSalutationOneRecipient (salutation, messageBody, messageBodyNew);
        } else if (StringUtils.isEmptyOrWhiteSpace (salutation) && !StringUtils.isEmptyOrWhiteSpace (secondSalutation)) {
            await this.addSalutationOneRecipient (secondSalutation, messageBody, messageBodyNew);
        } else if (! StringUtils.isEmptyOrWhiteSpace (salutation) && !StringUtils.isEmptyOrWhiteSpace (secondSalutation)) {
            await this.addSalutationTwoRecipients (secondSalutation, genderEmail, salutation, messageBody, messageBodyNew, genderSecondEmail);
        } else if (StringUtils.isEmptyOrWhiteSpace (salutation) && StringUtils.isEmptyOrWhiteSpace (secondSalutation)) {
            await this.addSalutationTwoRecipients (secondSalutation, genderEmail, salutation, messageBody, messageBodyNew, genderSecondEmail);
        }
    }


    /**
     * print one salutation and message body
     */
    async addSalutationOneRecipient (salutation, messageBody, messageBodyNew) {
        if (salutation) {
            if (messageBody.innerHTML.indexOf (salutation) === -1) {
                messageBody.innerHTML = `${salutation},` + '<br/>' + '<br/>' + messageBodyNew;
            }
        }
    }


    /**
     * print two salutations and message body
     * First salutation of female recipient
     */
    async addSalutationTwoRecipients (secondSalutation, genderEmail, salutation, messageBody, messageBodyNew, genderSecondEmail) {
        if (secondSalutation) {
            if (this.isGenderFemale (genderEmail)) {
                messageBody.innerHTML = this.buildBodyTwoRecipients (salutation, secondSalutation, messageBodyNew);
            } else if (this.isGenderFemale (genderSecondEmail)) {
                messageBody.innerHTML = this.buildBodyTwoRecipients (secondSalutation, salutation, messageBodyNew);
            } else {
                messageBody.innerHTML = this.buildBodyTwoRecipients (salutation, secondSalutation, messageBodyNew);
            }
        }
    }


    /**
     * if salutation does not exist in the database, "undefined" is output.
     * if "undefined" then do not print.
     */
    buildBodyTwoRecipients (first, second, messageBodyNew) {
        if (first === undefined && second !== undefined) {
            return `${second},` + '<br/>' + '<br/>' + messageBodyNew;
        } else if (first !== undefined && second === undefined) {
            return `${first},` + '<br/>' + '<br/>' + messageBodyNew;
        } else if (first === undefined && second === undefined) {
            return messageBodyNew;
        } else {
            return `${first},` + " " + `${second},` + '<br/>' + '<br/>' + messageBodyNew;
        }
    }


    buildBodyOneRecipient (first, messageBodyNew) {
        return `${first},` + '<br/>' + messageBodyNew;
    }


    isGenderFemale (genderString) {
        return genderString === "f";
    }


    /**
     * @param recipientIndex - index of recipient (1 for first recipient)
     */
    getEmailFromRecipient (recipientIndex) {
        let recipient = this.doc.getElementById (`addressCol2#${recipientIndex}`); //only use second recipient
        return recipient ? StringUtils.extractEmailAddress (recipient.value) : undefined;
    }

    async createSalutationCheckBox () {
        this.menuList = this.doc.createElement ("toolbarbutton");
        this.menuList.setAttribute ("id", "salutationSettings");
        this.menuList.setAttribute ("type", "menu");
        this.menuList.setAttribute ("sizetopopup", "none");
        await this.updateSalutationSettings ();

        let menuPopup = this.doc.createElement ("menupopup");

        let menuItemEnabled = this.doc.createElement ("menuitem");
        menuItemEnabled.id = "salutationEnabled";
        menuItemEnabled.setAttribute ("label", "Enable");
        menuItemEnabled.setAttribute ("enableSalutation", "true");
        menuPopup.appendChild (menuItemEnabled);

        let menuItemDisabled = this.doc.createElement ("menuitem");
        menuItemDisabled.id = "salutationDisabled";
        menuItemDisabled.setAttribute ("label", "Disable");
        menuItemDisabled.setAttribute ("enableSalutation", "false");
        menuPopup.appendChild (menuItemDisabled);

        this.menuList.appendChild (menuPopup);
        this.menuList.addEventListener ("command", (e) => this.onSalutationSettingsChange (e));

        this.doc.getElementById ("statusText").appendChild (this.menuList);
    }

    async onSalutationSettingsChange (e) {
        if (e.target.getAttribute ("enableSalutation") === "true") {
            await this.settingsService.updateIsSalutationEnabled ("true");
        }
        else {
            await this.settingsService.updateIsSalutationEnabled ("false");
        }
        await this.updateSalutationSettings ();
    }

    async updateSalutationSettings () {
        let isSalutationEnabled = await this.settingsService.IsSalutationEnabled ();
        this.enableSalutation = isSalutationEnabled;
        let enabledText = isSalutationEnabled ? "Enabled" : "Disabled";
        this.menuList.setAttribute ("label", "Salutation Insertion " + enabledText);
    }
}