# Software Projekt: SmartMail – a Thunderbird Add-On
This ReadMe is for every developer, who might be interessted in the dokumentation for our program and the workflow. 
You might read through this because you are writting some similar program.

First of all, what is the function of this program? It reconizes the gender of the person you are writing with and shows you this result. There are a few different 
cases we can recognize. You could write with a female, male or a group. If you did not write with the person before it will be checked if the first name is male or 
female. It could also be a unisex name or not recognized by our system. In this case you can add it to your personal database with clicking on the label until you 
see the gender the person has. 

Furthermore the program is able to store the language settings of the person you are writing with to optimize the workflow. Therefor the program recognize the language of a sender,
above the chosen language-settings and store these settings in the database. It is important that the intended additional language packets  "dictionaries" are installed,
otherwise the program just knows the default language of the Thunderbird user.
To install such dictionaries on Thunderbird you have to do the following steps

# Developer Setup
1. Go to the Thunderbird extensions folder.
 * Windows: %APPDATA%\Thunderbird\Profiles\<Profile Name>\extensions\
 * Linux: ~/.thunderbird/<Profile Name>/extensions/
 * MAC: ~/Library/Thunderbird/Profiles/<Profile Name>/extensions/

2. Create a file with the file name set as the value of `<em:id\>` from `install.rdf`. In our case `smartmail@fra-uas.de`.

3. Insert the full path of the `smartmail@fra-uas.de` directory in your cloned repo as the only line of text. (Example: `/Users/mrburns/Developer/smart-mail/smartmail@fra-uas.de`)

4. Run `yarn` after first cloning the repo. This assumes you have installed [yarn](https://yarnpkg.com).

5. Run `yarn transpile` to transpile to standard compliant ES2015 JavaScript.

6. The plugin is now ready. Restart Thunderbird. It should be enabled by default.


# Selbst signiertes SSL der Uni ignorieren/akzeptieren
<code>git -c http.sslVerify=false clone https://<Benutzername>@10.18.2.28/godehardt/smart-mail.git</code>

bzw. für den eigenen User
<code>git config --global http.sslVerify false</code>

# How to start with Thunderbird Extensions
* https://developer.mozilla.org/en-US/Add-ons/Thunderbird/Building_a_Thunderbird_extension

# Informationen zum Thema SQL
* https://developer.mozilla.org/en-US/Add-ons/Thunderbird/HowTos/Common_Thunderbird_Extension_Techniques/Use_SQLite
* https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Storage
* https://stackoverflow.com/questions/34227200/querying-sqlite-database-in-thunderbird
* https://developer.mozilla.org/en-US/docs/Mozilla/JavaScript_code_modules/Sqlite.jsm

# Offene Probleme (für Folgeprojekt)
* Die Suche nach dem Vornamen in den Daten der Mail-Adresse muss noch verbessert werden
  * Besonders Namen mit Umlauten sind noch problematisch
  * Hier muss eine Lösung für verschiedene Sprachen gefunden werden
  * Im deutschen sind das z.B. Namen wie Günther (Mail-Adresse: guenther, gunther o.ä.)
  * In anderen Sprachen kann es noch weitere Umlaute bzw. Sonderzeichen geben
* weitere Sprach-Datenbanken implementieren

# [npm](https://www.npmjs.com)
No, because Mozilla sucks. 🤮💩

https://discourse.mozilla.org/t/how-do-i-know-if-a-node-js-library-will-work-in-my-add-on/6845/7

# Debugging ES2017 / Babel / source maps
Transpilierter Code mit Babel kann debugged werden, source maps werden unterstützt: [Guide](https://developer.mozilla.org/en-US/docs/Tools/Debugger/How_to/Use_a_source_map)


# CheatSheets Yarn / Git / JavaScript (Ecmascript 6)

Yarn: https://www.cheatography.com/gregfinzer/cheat-sheets/yarn-package-manager/pdf/

Git: 
*  https://ndpsoftware.com/git-cheatsheet.html
*  https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf
*  http://files.zeroturnaround.com/pdf/zt_git_cheat_sheet.pdf
*  https://raw.githubusercontent.com/hbons/git-cheat-sheet/master/preview.png
*  http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

JavaScript
*  http://www.cheat-sheets.org/saved-copy/jsquick.pdf
*  https://www.cheatography.com/davechild/cheat-sheets/javascript/pdf/
*  http://www.cheat-sheets.org/saved-copy/javascript_cheat_sheet.pdf
*  https://www.tutorialspoint.com/javascript/index.htm
*  https://www.tutorialspoint.com/nodejs/
*  https://www.tutorialspoint.com/es6/


# transpile babel process:

`yarn`
`yarn transpile`

to setup WebStorm Filewatcher:

Settings 
-> search for FileWatcher
-> + Babel Watcher
Program: Path to your babel binary in node_modules/.bin/babel
Arguments: `-x .es6 smartmail@fra-uas.de --ignore /node_modules/ -d smartmail@fra-uas.de --source-maps --presets env`
Output path to refresh: `$FileDirRelativeToProjectRoot$\$FileNameWithoutExtension$.js:dist\$FileDirRelativeToProjectRoot$\$FileNameWithoutExtension$.js.map`


# update all devDependencies and update Dependencies

user script update with:
`yarn update`

- to update all dependencies necessary for development
- to update all dependencies necessary for execution (./smartmail@fra-uas.de/chrome/content/node_modules)

this makes dependencies available for plugin scripts at runtime

# Backend
Server @ http://smartmail-backend.herokuapp.com/

GitHub Repo @ https://github.com/godehardt/smartmail-backend

The backend is running on [Heroku](http://heroku.com).

Please contact @godehardt for invites to the GitHub repo and Heroku access.


## API

Please find the API documentation on the `/docs` endpoint.<br/>
https://smartmail-backend.herokuapp.com/docs/

## Updating the name database

There is a unix-executable (python3 script) `updateDatabase`(`<clonedRepo>/updateDatabase`) for updating the names database file (`names.sqlite`). You have to provide three environment variables. You will get the correct values by logging in to Heroku, revealing the config var `MONGODB_URI` under the project settings tab:
* `MONGO_URL = mongodb://heroku_wvvr2hvc:o7hefaoukk29jg04ebt5q90cfk@ds131546.mlab.com:31546/heroku_wvvr2hvc`
* `MONGO_DB = heroku_wvvr2hvc`
* `SQLITE_FILE_PATH = static/names.sqlite`

Don't forget to commit the updated `names.sqlite` to GitHub which will trigger a new Heroku build.
