# -*- coding: utf-8 -*-
from pprint import pprint 
import re 
import ast
import sqlite3
import sys
import csv
import os

def main(language):
    print('lauf fuer sprache: ', language )
    global namelist
    namelist = {}

    os.remove("databases/names_%s.sqlite" % language)
    # datenbank name verbinden
    conn = sqlite3.connect("databases/names_%s.sqlite" % language)
    c = conn.cursor()
    # setze encoding der datenbank auf utf-8
    c.execute('PRAGMA encoding="UTF-8";')

    # erstellen einer tabelle 
    c.execute('''CREATE TABLE IF NOT EXISTS names (name text primary key, gender text)''')

    # Namelists from https://petscan.wmflabs.org/
    # de Kategorien: Männlicher Vorname / Weiblicher Vorname
    # en Categories: Feminine_given_names / Masculine given names

    csv_folder = "csv_exports/"
    m_file = "%s%s_m.csv" % (csv_folder, language)
    f_file = "%s%s_f.csv" % (csv_folder, language)

    for gender in ['m', 'f']:
        with open('csv_exports/' + language + '_' + gender + '.csv') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in reader:
                checkDouble(row[1], gender)

    #pprint(namelist)
    print(len(namelist.keys()))

    for key, value in namelist.iteritems():
        # schreibe jeden eintrag in datanbank 
        insert = "INSERT INTO names VALUES ('" + key + "', '" + value + "')"
        try:
            c.execute(insert)
        except sqlite3.Error as e:
            print "An error occurred:", e.args[0]
            print(key, ' ', value)

    conn.commit()
    conn.close()
    print("done: ", language)
    namelist = {}

def checkDouble(name, gender):
    name = name.split('_')[0].lower().replace('\'', '')
    if name in namelist and gender != namelist[name]:
        namelist[name] = 'u'
        #print('Name: ' + name + ' is unisex')
    else:
        namelist[name] = gender

for lang in ['de', 'en']:
    main(lang)
