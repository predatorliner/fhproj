# -*- coding: utf-8 -*-
from pprint import pprint 
import re 
import sqlite3
import sys

#datenbank name verbinden
conn = sqlite3.connect("turk_names.db")
c = conn.cursor()

namelist = { 'name' : 'sulu' , 'gender' : 'm'}

def main():
	#erstellen einer tabelle 
	#c.execute('''CREATE TABLE turk (name text primary key, gender text)''')

	file = open("turk_names_gender.csv", 'r')
	for namegender in file:
	    namegender = namegender.split(',')
	    name = namegender[0].replace('"', '').replace('.', '').lower()
	    gender = namegender[1].replace('\n', '').replace('K', 'f').replace('E', 'm')

	    # wenn ein leerzeichen im string ist haben wir einen doppelnamen
	    if ' ' in name:
		for name in name.split(' '):
			if len(name)>1:
			    checkDouble(name, gender)
	    else:
		checkDouble(name, gender)
	    #print(name + " " + gender)

        pprint(namelist)
        print(len(namelist.keys()))

        for key, value in namelist.iteritems():
	    #schreib jeden eintrag in datanbank 
	    insert = "INSERT INTO turk VALUES ('" + key + "', '" + value + "')"
            #print(insert)
	    c.execute(insert)

	conn.commit()
	conn.close()
	print("done")

def checkDouble(name, gender):
    if name in namelist:
        if namelist['gender'] != gender:
            namelist[name] = 'u'
    else:
        namelist[name] = gender

main()
